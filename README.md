# LaboGit

## Comment croyez-vous qu’un système de gestion de versions comme Git améliorera votre productivité ?

Je pourrai utiliser git pour modifier mes articles scientifiques en cours de rédaction et ainsi éviter la confusion qui vient avec plusieurs fichiers tous nommés une variation du nom d'origine, et même créer différentes branches pour des modifications en attente d'approbation que je ne veux pas inclure tout de suite dans le projet principal. 
